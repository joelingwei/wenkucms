-- phpMyAdmin SQL Dump
-- version phpStudy 2014
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2019 �?01 �?14 �?06:21
-- 服务器版本: 5.5.53
-- PHP 版本: 5.6.27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `wmcms`
--

--
-- 转存表中的数据 `wk_ad`
--

INSERT INTO `wk_ad` (`id`, `board_id`, `type`, `name`, `url`, `content`, `extimg`, `extval`, `desc`, `start_time`, `end_time`, `clicks`, `add_time`, `ordid`, `status`) VALUES
(1, 2, 'image', '文档页预览20秒 - 暂未增加', 'http://www.qizhixiong.com', '', '', '', '', 1527523200, 1906214400, 0, 0, 255, 0),
(8, 10, 'image', '列表页导航下	', 'http://www.qizhixiong.com', '1810/14/5bc2ea861a5f3.png', '', '', '', 1527523200, 1906214400, 0, 0, 255, 0),
(4, 4, 'image', '底部广告', 'http://www.qizhixiong.com', '1810/14/5bc2ea986552b.png', '', '', '', 1527523200, 1906214400, 0, 0, 255, 0),
(6, 9, 'image', '首页幻灯片下', 'http://www.qizhixiong.com', '1810/14/5bc2eaa67187b.png', '', '', '', 1527523200, 1906214400, 0, 0, 255, 0),
(9, 11, 'image', '发布页右侧', 'http://www.qizhixiong.com', '1810/14/ad.png', '', '', '', 1527523200, 1906214400, 0, 0, 255, 0),
(10, 12, 'image', '新闻列表页右侧', 'http://www.qizhixiong.com', '1810/14/ad.png', '', '', '', 1527523200, 1906214400, 0, 0, 255, 0),
(12, 13, 'image', '新闻内容右侧', 'http://www.qizhixiong.com', '1810/14/ad.png', '', '', '', 1527523200, 1906214400, 0, 0, 255, 0),
(13, 14, 'image', '文档列表页右侧', 'http://www.qizhixiong.com', '1810/14/ad.png', '', '', '', 1527523200, 1906214400, 0, 0, 255, 0),
(14, 15, 'image', '文档内容页右侧', 'http://www.qizhixiong.com', '1810/14/ad.png', '', '', '', 1527523200, 1906214400, 0, 0, 255, 0);

--
-- 转存表中的数据 `wk_adboard`
--

INSERT INTO `wk_adboard` (`id`, `name`, `tpl`, `width`, `height`, `description`, `status`) VALUES
(2, '文档预览20s广告', 'indexfocus', 0, 0, '', 1),
(9, '首页幻灯片下方横幅', 'banner', 0, 0, '', 1),
(4, '底部横幅', 'banner', 0, 0, '', 1),
(10, '列表页导航下横幅', 'banner', 0, 0, '', 1),
(11, '发布页右侧广告位', 'banner', 0, 0, '', 1),
(12, '新闻列表页右侧', 'banner', 0, 0, '', 1),
(13, '新闻内容页右侧', 'banner', 0, 0, '', 1),
(14, '文档列表页右侧', 'banner', 0, 0, '', 1),
(15, '文档内容页右侧', 'banner', 0, 0, '', 1);

--
-- 转存表中的数据 `wk_admin_auth`
--

INSERT INTO `wk_admin_auth` (`role_id`, `menu_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 9),
(1, 10),
(1, 11),
(1, 13),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 24),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 36),
(1, 44),
(1, 45),
(1, 60),
(1, 61),
(1, 62),
(1, 65),
(1, 66),
(1, 67),
(1, 69),
(1, 70),
(1, 71),
(1, 72),
(1, 73),
(1, 74),
(1, 76),
(1, 77),
(1, 78),
(1, 81);

--
-- 转存表中的数据 `wk_article_cate`
--

INSERT INTO `wk_article_cate` (`id`, `name`, `img`, `pid`, `spid`, `ordid`, `status`) VALUES
(14, '企业资讯', '', 0, '0', 10, 1),
(15, '国内核讯', '', 0, '0', 20, 1),
(16, '科普知识', '', 0, '0', 30, 1),
(19, '聚焦核电', '', 0, '0', 40, 1),
(18, '政策法规', '', 0, '0', 50, 1);

--
-- 转存表中的数据 `wk_doc_cate`
--

INSERT INTO `wk_doc_cate` (`id`, `name`, `img`, `pid`, `spid`, `ordid`, `status`) VALUES
(36, '设备制造', '', 2, '2|', 255, 1),
(48, '总结汇报', '', 33, '33|', 255, 1),
(25, '其他考试', '', 23, '23|', 255, 1),
(33, '实用文档', '', 0, '0', 255, 1),
(35, '高温气冷堆技术等其他', '', 1, '1|', 255, 1),
(38, '电站运营', '', 2, '2|', 255, 1),
(39, '检修维护', '', 2, '2|', 255, 1),
(40, '中国标准', '', 3, '3|', 255, 1),
(43, '标准汇编', '', 3, '3|', 255, 1),
(42, '条例规章', '', 3, '3|', 255, 1),
(41, ' 核行业标准', '', 3, '3|', 255, 1),
(44, '其他行业标准', '', 3, '3|', 255, 1),
(46, '核行业研究', '', 10, '10|', 255, 1),
(47, '核应急', '', 10, '10|', 10, 1),
(49, '党工团纪', '', 33, '33|', 255, 1),
(57, '二代以内技术', '', 1, '1|', 10, 1),
(24, '核安全工程师', '', 23, '23|', 255, 1),
(23, '资格考试', '', 0, '0', 255, 1),
(45, ' 核能英语', '', 10, '10|', 255, 1),
(18, '质量安全', '', 10, '10|', 255, 1),
(34, 'EPR技术', '', 1, '1|', 255, 1),
(37, '工程建设及调试', '', 2, '2|', 255, 1),
(12, ' APCAP1400', '', 1, '1|', 255, 1),
(10, '通用文档', '', 0, '0', 300, 1),
(9, '国外标准', '', 3, '3|', 255, 1),
(7, '核燃料及后处理', '', 2, '2|', 255, 1),
(6, '华龙一号', '', 1, '1|', 255, 1),
(3, '行业标准', '', 0, '0', 255, 1),
(2, '建造区', '', 0, '0', 255, 1),
(1, '核电技术', '', 0, '0', 255, 1);

--
-- 转存表中的数据 `wk_flink`
--

INSERT INTO `wk_flink` (`id`, `name`, `img`, `url`, `cate_id`, `ordid`, `status`) VALUES
(14, '百度', '', 'http://www.baidu.com', 0, 10, 1),
(15, '网易', '', 'http://www.163.com', 0, 10, 1),
(16, '新浪', '', 'http://www.sina.com.cn', 0, 10, 1),
(17, '腾讯', '', 'http://www.qq.com', 0, 10, 1);

--
-- 转存表中的数据 `wk_flink_cate`
--

INSERT INTO `wk_flink_cate` (`id`, `name`, `ordid`, `status`) VALUES
(2, '文字链接', 255, 1);

--
-- 转存表中的数据 `wk_forum_cate`
--

INSERT INTO `wk_forum_cate` (`id`, `name`, `ordid`, `status`) VALUES
(2, '默认板块', 10, 1),
(3, '测试板块', 20, 1);

--
-- 转存表中的数据 `wk_global`
--

INSERT INTO `wk_global` (`name`, `data`, `remark`) VALUES
('web_switch', 'a:1:{s:7:"doc_con";s:1:"2";}', ''),
('score_name', '积分', ''),
('score_pay', 'a:3:{s:7:"isscore";s:1:"1";s:8:"getscore";s:2:"10";s:6:"adtime";s:2:"20";}', ''),
('score_item_img', 'a:4:{s:6:"bwidth";s:3:"350";s:7:"bheight";s:3:"350";s:6:"swidth";s:3:"210";s:7:"sheight";s:3:"210";}', ''),
('reg_closed_reason', '<h1>网站维护中</h1>', ''),
('reg_status', '1', ''),
('attach_path', 'data/upload/', ''),
('user_intro_default', '这个家伙太懒，什么都木留下~', ''),
('statics_url', '', ''),
('verinfo', '0', ''),
('web_model', '3', ''),
('appsecret', '', ''),
('appkey', '', ''),
('doctype', '*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx;*.wps;*.et;*.pdf;*.txt;', ''),
('ipban_switch', '1', ''),
('avatar_size', '24,32,48,64,120,big', ''),
('attr_allow_exts', 'jpg,jpeg,png,gif,swf', ''),
('attr_allow_size', '20480', ''),
('seo_config', 'a:13:{s:5:"index";a:3:{s:5:"title";s:37:"七只熊开源文库建站系统v3.2";s:8:"keywords";s:14:"首页Keywords";s:11:"description";s:17:"首页Description";}s:7:"doclist";a:3:{s:5:"title";s:14:"列表页Title";s:8:"keywords";s:17:"列表页Keywords";s:11:"description";s:20:"列表页Description";}s:6:"doccon";a:3:{s:5:"title";s:14:"内容页Title";s:8:"keywords";s:17:"内容页Keywords";s:11:"description";s:20:"内容页Description";}s:7:"taglist";a:3:{s:5:"title";s:14:"标签页Title";s:8:"keywords";s:17:"标签页Keywords";s:11:"description";s:20:"标签页Description";}s:7:"ucenter";a:3:{s:5:"title";s:17:"个人中心Title";s:8:"keywords";s:20:"个人中心Keywords";s:11:"description";s:23:"个人中心Description";}s:6:"member";a:3:{s:5:"title";s:17:"用户查看Title";s:8:"keywords";s:20:"用户查看Keywords";s:11:"description";s:23:"用户查看Description";}s:6:"search";a:3:{s:5:"title";s:14:"搜索页Title";s:8:"keywords";s:17:"搜索页Keywords";s:11:"description";s:20:"搜索页Description";}s:2:"zj";a:3:{s:5:"title";s:17:"专辑页面Title";s:8:"keywords";s:20:"专辑页面Keywords";s:11:"description";s:24:"专辑页面Description\n";}s:4:"page";a:3:{s:5:"title";s:11:"单页Title";s:8:"keywords";s:14:"单页Keywords";s:11:"description";s:17:"单页Description";}s:4:"news";a:3:{s:5:"title";s:21:"新闻列表页title2";s:8:"keywords";s:25:"新闻列表页关键字2";s:11:"description";s:15:"列表页title2";}s:7:"newscon";a:3:{s:5:"title";s:21:"新闻内容页Title1";s:8:"keywords";s:24:"新闻内容页Keywords1";s:11:"description";s:27:"新闻内容页Description1";}s:3:"txl";a:3:{s:5:"title";s:23:"企业名录列表Title";s:8:"keywords";s:26:"企业名录列表Keywords";s:11:"description";s:29:"企业名录列表Description";}s:6:"txlcon";a:3:{s:5:"title";s:24:" 企业名录内容Title";s:8:"keywords";s:27:" 企业名录内容Keywords";s:11:"description";s:30:" 企业名录内容Description";}}', ''),
('reg_protocol', '注册协议注册协议', ''),
('integrate_config', '', ''),
('integrate_code', 'default', ''),
('mail_server', 'a:6:{s:4:"mode";s:4:"smtp";s:4:"host";s:12:"smtp.126.com";s:4:"port";s:2:"25";s:4:"from";s:18:"wfgolovecc@126.com";s:13:"auth_username";s:10:"wfgolovecc";s:13:"auth_password";s:6:"221122";}', ''),
('score_rule', 'a:8:{s:8:"register";s:2:"10";s:5:"login";s:2:"10";s:10:"login_nums";s:1:"1";s:7:"docdown";s:1:"5";s:8:"docscore";s:1:"0";s:5:"docup";s:1:"0";s:6:"docdel";s:1:"5";s:5:"fatie";s:1:"9";}', '\n'),
('statistics_code', '', ''),
('site_icp', '京ICP备888888号', ''),
('site_description', '一个很好的文库系统', ''),
('site_status', '1', ''),
('closed_reason', '网站升级中。。', ''),
('site_keyword', '文库,好文库,PHP文库系统', ''),
('site_title', '七只熊-文库系统v3.2', ''),
('site_url', 'http://www.wenkucms.com/', ''),
('site_theme', 'blueideal', ''),
('auto_time', '1', ''),
('site_name', '七只熊文库', ''),
('convert_type', '1', ''),
('convert_appsecret_1', '', ''),
('convert_appid_1', '', ''),
('convert_site_1', 'http://convert.qizhixiong.com/', ''),
('convert_appsecret_2', '', ''),
('convert_appid_2', '', ''),
('convert_site_2', 'site', ''),
('site_icp', '京ICP备888888号', ''),
('site_tel', '13888888888', ''),
('site_qq', '996403', ''),
('site_tel', '13888888888', ''),
('gonggao_status', '0', ''),
('gonggao_protocol', '<div style="padding:20px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 340;"><h3>感谢您使用七只熊文库系统！</h3><br>我们提供完整的文档在线转HTML<br>和文档在线管理、预览服务！<br><br>· 我们的官网是：<a href="http://www.qizhixiong.com" style="color:#fff;" target="_blank">www.qizhixiong.com</a><br><br>· 带数据演示站：<a href="http://doc.qizhixiong.com" style="color:#fff;" target="_blank">doc.qizhixiong.com</a><br><br><br>关闭公告方法：<br>进入后台 -> 系统 -> 首页公告</div>', '');

--
-- 转存表中的数据 `wk_menu`
--

INSERT INTO `wk_menu` (`id`, `name`, `pid`, `module_name`, `action_name`, `data`, `remark`, `often`, `ordid`, `style`, `ico`) VALUES
(1, '系统', 0, 'global', 'index', '', '', 0, 10, 'left', 'xe614'),
(2, '界面', 0, 'view', 'index', '', '', 0, 20, 'left', 'xe670'),
(3, '用户', 0, 'user', 'index', '', '', 0, 30, 'left', 'xe612'),
(4, '文档', 0, 'content', 'index', '', '', 0, 40, 'left', 'xe60a'),
(5, '运营', 0, 'operation', 'index', '', '', 0, 50, 'left', 'xe637'),
(6, '工具', 0, 'tools', 'index', '', '', 0, 60, 'left', 'xe631'),
(7, '应用', 0, 'app', 'index', '', '', 0, 70, 'left', 'xe653'),
(9, '站点设置', 1, 'global', 'index', '', '', 0, 1, 'left', 'xe602'),
(10, '附件设置', 1, 'global', 'index', '&type=attach', '', 0, 2, 'left', 'xe602'),
(11, '邮件设置', 0, 'global', 'index', '&type=mail', '', 0, 3, '', 'xe602'),
(81, '审核开关', 1, 'global', 'index', '&type=switch', '', 0, 4, 'left', 'xe602'),
(13, '会员管理', 3, 'user', 'index', '', '', 0, 1, 'left', 'xe602'),
(15, '清理缓存', 0, 'cache', 'index', '', '', 0, 100, 'top', 'xe640'),
(16, '积分', 0, 'score', 'setting', '', '', 0, 35, 'left', 'xe658'),
(17, '积分统计', 16, 'score', 'logs', '', '', 0, 2, 'left', 'xe602'),
(18, '积分规则', 16, 'score', 'setting', '', '', 0, 1, 'left', 'xe602'),
(19, '注册选项', 1, 'global', 'index', '&type=register', '', 0, 5, 'left', 'xe602'),
(20, 'SEO', 0, 'seo', 'page', '', '', 0, 55, 'left', 'xe64d'),
(21, '用户组管理', 3, 'user_role', 'index', '', '', 0, 2, 'left', 'xe602'),
(22, '会员充值记录', 3, 'recharge', 'index', '', '', 0, 3, 'left', 'xe602'),
(30, '会员积分统计', 3, 'user_scoresum', 'index', '', '', 0, 5, 'left', 'xe602'),
(24, '导航设置', 2, 'nav', 'index', '', '', 0, 1, 'left', 'xe602'),
(26, '主导航', 24, 'nav', 'index', '', '', 0, 1, 'left', 'xe602'),
(27, '底部导航', 24, 'nav', 'index', '&type=bottom', '', 0, 2, 'left', 'xe602'),
(28, '登陆插件', 7, 'oauth', 'index', '', '', 0, 1, 'left', 'xe602'),
(29, '登陆整合', 0, 'integrate', 'index', '', '', 0, 3, '', 'xe602'),
(31, '支付接口', 7, 'payment', 'index', '', '', 0, 3, 'left', 'xe602'),
(32, '会员提现记录', 0, 'withdraw', 'index', '', '', 0, 4, '', 'xe602'),
(33, '数据备份', 6, 'backup', 'index', '', '', 0, 1, 'left', 'xe602'),
(34, '数据恢复', 6, 'backup', 'restore', '', '', 0, 2, 'left', 'xe602'),
(36, '黑名单管理', 0, 'ipban', 'index', '', '', 0, 3, '', 'xe602'),
(44, '伪静态设置', 20, 'seo', 'url', '', '', 0, 2, 'left', 'xe602'),
(45, 'SEO设置', 20, 'seo', 'page', '', '', 0, 1, 'left', 'xe602'),
(60, '广告管理', 5, 'ad', 'index', '', '', 0, 0, 'left', 'xe602'),
(61, '广告位管理', 5, 'adboard', 'index', '', '', 0, 2, 'left', 'xe602'),
(62, '友情链接', 5, 'flink', 'index', '', '', 0, 2, 'left', 'xe602'),
(65, '模版设置', 2, 'view', 'tpl', '', '', 0, 2, 'left', 'xe602'),
(67, '文档列表', 4, 'doc_con', 'index', '', '', 0, 3, 'left', 'xe602'),
(99, '文档标签', 4, 'tag', 'index', '', '', 0, 10, 'left', 'xe602'),
(98, '帖子管理', 96, 'forum', 'index', '', '', 0, 10, 'left', 'xe655'),
(76, '系统通知', 0, 'user_mail', 'index', '', '', 0, 90, 'top', 'xe613'),
(77, '文档分类', 4, 'doc_cate', 'index', '', '', 0, 5, 'left', 'xe602'),
(82, '新闻', 0, 'article', 'index', '', '', 0, 45, 'left', 'xe705'),
(83, '新闻列表', 82, 'article', 'index', '', '', 0, 3, 'left', 'xe602'),
(84, '新闻分类', 82, 'article_cate', 'index', '', '', 0, 5, 'left', 'xe602'),
(85, '文档专辑', 4, 'zj', 'index', '', '', 0, 5, 'left', 'xe602'),
(86, '转换设置', 1, 'global', 'index', '&type=convert', '', 0, 5, 'left', 'xe602'),
(87, '幻灯片管理', 5, 'slide', 'index', '', '', 0, 2, 'left', 'xe602'),
(88, '举报管理', 5, 'jubao', 'index', '', '', 0, 3, 'left', 'xe602'),
(89, '单页', 0, 'page', 'index', '', '', 0, 45, 'left', 'xe655'),
(90, '单页管理', 89, 'page', 'index', '', '', 0, 10, 'left', 'xe655'),
(91, '企业名录', 0, 'txl', 'index', '', '', 0, 48, 'left', 'xe857'),
(92, '企业列表', 91, 'txl', 'index', '', '', 0, 3, 'left', 'xe602'),
(93, '地区管理', 91, 'txl_cate', 'index', '', '', 0, 3, 'left', 'xe602'),
(94, '纠错', 91, 'txl_jiucuo', 'index', '', '', 0, 3, 'left', 'xe602'),
(95, '首页公告', 1, 'global', 'index', '&type=gonggao', '', 0, 5, 'left', 'xe602'),
(96, '论坛', 0, 'forum', 'index', '', '', 0, 47, 'left', 'xe655'),
(97, '论坛板块', 96, 'forum_cate', 'index', '', '', 0, 10, 'left', 'xe655');

--
-- 转存表中的数据 `wk_nav`
--

INSERT INTO `wk_nav` (`id`, `type`, `name`, `alias`, `link`, `target`, `ordid`, `mod`, `status`) VALUES
(7, 'bottom', '商业会员', '商业会员', 'index.php?m=page&a=index&id=3', 1, 10, '', 1),
(8, 'bottom', '积分获取', '积分获取', 'index.php?m=page&a=index&id=4', 1, 20, '', 1),
(9, 'bottom', '关于我们', '关于我们', 'index.php?m=page&a=index&id=1', 1, 5, '', 1),
(10, 'bottom', '免责声明', '免责声明', 'index.php?m=page&a=index&id=2', 1, 50, '', 1),
(11, 'bottom', ' 联系我们', ' 联系我们', 'index.php?m=page&a=index&id=5', 1, 40, '', 1),
(14, 'main', '外链网易', 'wangyi', 'http://www.163.com', 1, 10, '', 1);

--
-- 转存表中的数据 `wk_oauth`
--

INSERT INTO `wk_oauth` (`id`, `code`, `name`, `config`, `desc`, `author`, `ordid`, `status`) VALUES
(15, 'wechat', '微信登录', 'a:2:{s:7:"app_key";s:0:"";s:10:"app_secret";s:0:"";}', '申请地址：http://open.weixin.qq.com/', '七只熊', 0, 1),
(14, 'qq', 'QQ登录', 'a:2:{s:7:"app_key";s:0:"";s:10:"app_secret";s:0:"";}', '申请地址：http://connect.opensns.qq.com/', '七只熊', 0, 1),
(12, 'sina', '新浪微薄登录', 'a:2:{s:7:"app_key";s:0:"";s:10:"app_secret";s:0:"";}', '申请地址：http://open.weibo.com/', '七只熊', 0, 1),
(16, 'taobao', '淘宝登录', 'a:2:{s:7:"app_key";s:0:"";s:10:"app_secret";s:0:"";}', '申请地址：http://open.taobao.com/', '七只熊', 0, 1);

--
-- 转存表中的数据 `wk_page`
--

INSERT INTO `wk_page` (`id`, `title`, `content`, `ordid`, `status`, `add_time`) VALUES
(1, '关于我们', '<p>关于我们关于我们关于我们关于我们</p>', 10, 1, 0),
(2, '免责声明', '<p>免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明免责声明</p>', 10, 1, 0),
(3, '商业会员', '<p>商业会员商业会员商业会员商业会员商业会员商业会员商业会员商业会员商业会员商业会员商业会员商业会员商业会员</p>', 10, 1, 0),
(4, '积分获取', '<p>积分获取积分获取积分获取积分获取积分获取积分获取积分获取积分获取积分获取积分获取积分获取积分获取</p>', 10, 1, 0),
(5, '联系我们', '<p>联系我们联系我们联系我们联系我们联系我们联系我们联系我们联系我们联系我们联系我们联系我们联系我们</p>', 10, 1, 0),
(6, '文档发布协议', '<p>文档发布协议文档发布协议文档发布协议文档发布协议文档发布协议文档发布协议文档发布协议文档发布协议文档发布协议文档发布协议文档发布协议文档发布协议文档发布协议文档发布协议文档发布协议文档发布协议</p>', 10, 1, 0),
(7, '网站注册协议', '<p>使用协议使用协议使用协议使用协议使用协议使用协议使用协议sss</p>', 10, 1, 0);

--
-- 转存表中的数据 `wk_payment`
--

INSERT INTO `wk_payment` (`id`, `name`, `mark`, `description`, `logo`, `merchant`, `account`, `key`, `ordid`, `status`, `appid`, `appsecret`) VALUES
(1, '支付宝', 'Alipay', '支付宝 知托付！', '5ba47ce5eaf68.png', '2088401940700805', 'fenggouwang@foxmail.com', 'dben5611crju4vvk8dr0kjl6krcf6vb9', 1, 1, '2088401940700805', 'dben5611crju4vvk8dr0kjl6krcf6vb9'),
(2, '财付通', 'Tenpay', '会支付 会生活!', '', '2088401940700805', 'fenggouwang@foxmail.com', 'dben5611crju4vvk8dr0kjl6krcf6vb9', 0, 1, '2088401940700805', 'dben5611crju4vvk8dr0kjl6krcf6vb9'),
(3, '微信', 'Wechat', '微信支付!', '', '2088401940700805', 'fenggouwang@foxmail.com', 'dben5611crju4vvk8dr0kjl6krcf6vb9', 1, 1, 'wx04f389bb36b3df06', '75913d186203010da0a67765a52ac158');

--
-- 转存表中的数据 `wk_txl_cate`
--

INSERT INTO `wk_txl_cate` (`id`, `name`, `ordid`, `status`) VALUES
(1, '北京', 10, 1),
(2, '上海', 20, 1),
(4, '深圳', 30, 1);

--
-- 转存表中的数据 `wk_user_role`
--

INSERT INTO `wk_user_role` (`id`, `name`, `score`, `status`, `isAdmin`) VALUES
(1, '管理员', 0, 1, 1),
(2, '网站会员', 0, 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
